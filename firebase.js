import * as firebase from 'firebase';

let config = {
    apiKey: "AIzaSyAdfL5sRoKnDOWvA90-VO0UYeE83PmOp6U",
    authDomain: "ilemona-c425b.firebaseapp.com",
    databaseURL: "https://ilemona-c425b.firebaseio.com",
    projectId: "ilemona-c425b",
    storageBucket: "ilemona-c425b.appspot.com",
    messagingSenderId: "74770933564",
    appId: "1:74770933564:web:a5bd39d24cb50be84ee039",
    measurementId: "G-TX53WX13FL"
};
firebase.initializeApp(config);

export default firebase;