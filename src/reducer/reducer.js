import {fromJS} from 'immutable';
const initialState = fromJS({
    pigments: [],
    fillers: [],
    finalPercentages: [],
    results: [],
    unit: 'g',
    savedFormulae: {},
    refresh: true
})
export default (state = initialState, action) => {

    switch (action.type){
        
        case 'ADD_PIGMENTS':
            return state.set('pigments',action.payload)

        case 'ADD_FILLERS':
            return state.set('fillers',action.payload)
        
        case 'ADD_FINAL_PERCENTAGES':
            return state.set('finalPercentages',action.payload)
        
        case 'CHANGE_UNIT':
            return state.set('unit', action.payload)

        case 'SAVED_FORMULAE':
            return state.set('savedFormulae', action.payload)

        case 'REFRESH':
            return state.set('refresh', action.payload)
    
        default:
            return state
    }
}