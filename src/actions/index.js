export const addPigments = (values) => {
    return {
        type: 'ADD_PIGMENTS',
        payload: values
    }
}

export const addFillers = (values) => {
    return {
        type: 'ADD_FILLERS',
        payload: values
    }
}

export const addFinalPercentages = (values) => {
    return {
        type: 'ADD_FINAL_PERCENTAGES',
        payload: values
    }
}

export const changeUnit = (unit) => {
    return {
        type: 'CHANGE_UNIT',
        payload: unit
    }
}

export const loadSavedFormulae = (formulae) => {
    return {
        type: 'SAVED_FORMULAE',
        payload: formulae
    }
}

export const refreshApp = (value) => {
    return {
        type: 'REFRESH',
        payload: value
    }
}