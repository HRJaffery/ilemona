import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context'

import SplashScreen from './screens/SplashScreen'
import SignupScreen from './screens/SignupScreen'
import SigninScreen from './screens/SigninScreen'
import AppHomeScreen from './screens/AppHomeScreen'
import PigmentScreen from './screens/PigmentScreen'
import FillerScreen from './screens/FillerScreen'
import FinalScreen from './screens/FinalScreen'
import ResultsScreen from './screens/Results'
import MyFormulaeScreen from './screens/MyFormulae'
import ViewFormulaScreen from './screens/ViewFormula'
import SettingsScreen from './screens/SettingsScreen'
import HintsScreen from './screens/HintsScreen'

import CustomDrawer from './components/CustomDrawer'

const navigator = createSwitchNavigator(
  {
    Splash: SplashScreen,

    loginFlow: createStackNavigator({
      Signin: SigninScreen,
      Signup: SignupScreen,
    },
      {
        headerMode: false
      }),
    mainFlow: createStackNavigator({
      drawerFlow: createDrawerNavigator({
        AppHome: AppHomeScreen,
        Pigment: PigmentScreen,
        Filler: FillerScreen,
        Final: FinalScreen,
        Results: ResultsScreen,
        MyFormulae: MyFormulaeScreen,
        ViewFormula: ViewFormulaScreen,
        Settings: SettingsScreen,
        Hints: HintsScreen,
      },
        {
          contentComponent: CustomDrawer
        }),
    },
      {
        headerMode: false
      }),
  },
  {
    headerMode: false,
    initialRouteName: 'Splash',
  }
)
const AppContainer = createAppContainer(navigator)

const StackNavigator = () => {
  return (
    // <Provider store={createStore(reducers)}>
    <SafeAreaProvider>
      <SafeAreaView style={{ flex: 1 }}>
        <AppContainer />
      </SafeAreaView>
    </SafeAreaProvider>
    // </Provider>
  )
}

export default StackNavigator