import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'
import { colors } from '../configs';

export default function DrawerItem  ({callback, icon, iconLib, label}) { 
    return (
        <TouchableOpacity style={styles.textOpacity} onPress={()=>callback()}>
            <Text style={styles.text}>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    
    text: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 5,
        color: colors.purpleDark
    },
    textOpacity: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
    }
})