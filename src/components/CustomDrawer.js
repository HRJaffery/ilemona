import React from 'react'
import { View, StyleSheet, AsyncStorage } from 'react-native'
import LogoHeader from './LogoHeader'
import DrawerItem from './DrawerItem'

const CustomDrawer = ({ navigation }) => {

    const AppHome = () => {
        navigation.closeDrawer()
        navigation.navigate('AppHome')
    }
    const MyFormulae = () => {
        navigation.closeDrawer()
        navigation.navigate('MyFormulae')
    }
    const Settings = () => {
        navigation.closeDrawer()
        navigation.navigate('Settings')
    }
    const Hints = () => {
        navigation.closeDrawer()
        navigation.navigate('Hints')
    }
    const logout = async () => {
        await AsyncStorage.removeItem('token')
        await AsyncStorage.removeItem('uid')
        navigation.closeDrawer()
        navigation.navigate('Signin')
        
    }
    return (

        <View style={styles.main}>

            <View style={styles.banner}>
                <LogoHeader />
            </View>

            <DrawerItem icon='home' label='Home' callback={AppHome} />
            <DrawerItem icon='shopping-cart' label='My Formulae' callback={MyFormulae} />
            <DrawerItem icon='history' label='Settings' callback={Settings} />
            <DrawerItem icon='history' label='Hints' callback={Hints} />
            <DrawerItem icon='help-outline' label='Logout' callback={logout} />

            <View style={{ flex: 1 }} />
        </View>
    )
}

const styles = StyleSheet.create({
    banner: {
        height: 180,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    },
    main: {
        flex: 1
    },
})

export default CustomDrawer