import React from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { colors } from '../configs';

const InputField = ({ title, value, unit, callBack }) => {
    return (
        <View style={styles.view}>

            <View style={styles.titleSpace}>
                <Text style={styles.titleText}>{title}</Text>
            </View>

            <View style={styles.inputSpace}>

                <View style={styles.textInput}>
                    <TextInput keyboardType='numeric' value={value} selectionColor={colors.purpleDark} placeholder={"Qty."} placeholderTextColor={colors.pinkLight2} style={{ color: colors.purpleDark, fontSize: 20 }} autoCapitalize="none" onChangeText={(val) => callBack(val)} />

                    <TextInput />
                </View>
                <View style={styles.unit}>
                    <Text style={styles.unitText}>{unit}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        height: 50,
        borderRadius: 20,
        borderTopLeftRadius: 20,
        marginVertical: 5
    },
    titleSpace: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderTopLeftRadius: 20,
        backgroundColor: colors.pinkLight,
        justifyContent: 'center',
        flex: 2,
        paddingLeft: 10
    },
    titleText: {
        color: "white",
        fontSize: 20
    },
    inputSpace: {
        flexDirection: 'row',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        borderColor: colors.pinkLight,
        borderWidth: 1,
        flex: 1,
        paddingHorizontal: 5
    },
    unit: {
        justifyContent: 'center',
        marginHorizontal: 5
    },
    textInput: {
        flex: 1,
    },
    unitText: {
        color: colors.purpleDark,
        fontSize: 20
    },
    header: {

        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        padding: 5,
        backgroundColor: colors.purpleDark,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,

    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        color: 'white',
        fontFamily: "times",
        fontSize: 24
    },
    rightIcon: {
        padding: 0,
        marginRight: 5,
        paddingVertical: 5,
        paddingLeft: 8,
        paddingRight: 3,
    }
})

export default InputField