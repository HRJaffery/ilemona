import React, { Component } from "react";
import { StyleSheet, AsyncStorage} from "react-native";
import SectionedMultiSelect from './react-native-sectioned-multi-select-master'
import {connect} from 'react-redux'
import { colors } from '../configs'
import * as actions from '../actions'
class Dropdown extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      options: this.createOptionsArray()
    };
  }
  
  onSelectedItemsChange = async(selectedItems) => {
    this.setState({ selectedItems });
    this.props.changeUnit(this.props.options[selectedItems])
    let list = this.props.options
    await AsyncStorage.setItem("unit",this.props.options[selectedItems])
  };
  
  createOptionsArray = () => {
    let list = this.props.options
    let children = []
    let i = 0
    list.forEach(element => {
      let obj = {}
      obj["name"] = element
      obj["id"] = i
      i = i+1
      children.push(obj)
    });
    let options = [
      {
        name: "Options",
        id: -1,
        children: children
      }
    ]
    return options
  }

  render() {
    return (
        
        <SectionedMultiSelect
          items={this.state.options}
          hideSearch = {true}
          uniqueKey="id"
          subKey="children"
          selectText="Please select the unit you want to use"
          showDropDowns={false}
          readOnlyHeadings={true}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={this.state.selectedItems}
          showChips={false}
          single = {true}
          styles = {styles}
          colors = {{selectToggleTextColor: colors.purpleDark}}
        
        />

    );
  }
}
const styles = StyleSheet.create({
  view: {
    marginVertical: 1,
    marginHorizontal: 5,
    borderColor: 'red',
    borderWidth: 10
  },
  listContainer   : {
    borderColor: 'red',
    borderWidth: 10,
    backgroundColor: 'red'
  },
  button: {
      backgroundColor: colors.purpleDark
  }

})

export default connect(null,actions)(Dropdown)
