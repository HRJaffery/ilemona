import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Keyboard} from 'react-native'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { colors } from '../configs';


const Header = ({leftIcon,leftCallback,title}) => {
return (
    <>
    
    <View style={styles.header}>
        {leftIcon ?
        <TouchableOpacity style={{marginRight: 10, padding: 5}} 
        onPress={()=>{
            Keyboard.dismiss()
            leftCallback()
        }}>
            <MaterialIcons name={leftIcon} color='white' size={30}/>
        </TouchableOpacity>
        : null
        }
         
        <View style={{flex: 1}}>
            <Text style={styles.title}>{title}</Text>   
        </View>
        <TouchableOpacity style={{marginRight: 20, padding: 15}} 
        onPress={()=>{
           
        }}>
            {/* <MaterialIcons name={leftIcon} color='white' size={30}/> */}
        </TouchableOpacity>
    </View>
    
    </>
    )
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        padding: 5,
        backgroundColor: colors.pinkLight,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        elevation: 2,
  
    },
    header: {
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        padding: 5,
        backgroundColor: colors.purpleDark,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        color: 'white',
        fontFamily: "times",
        fontSize: 24
    },
    rightIcon: {
        padding: 0,
        marginRight: 5,
        paddingVertical:5,
        paddingLeft:8,
        paddingRight: 3,
    }
})

export default Header