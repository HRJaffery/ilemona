import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Keyboard,
  ScrollView,
  Dimensions,
  Text
} from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import InputField from '../components/InputField'
import { colors } from '../configs'

const PigmentScreen = ({ navigation, addPigments, state }) => {

  useEffect(() => {
   setVal1('')
   setVal2('')
   setVal3('')
   setVal4('')
   setVal5('')
   setVal6('')
   setVal7('')
   setVal8('')
    setError('')

  }, [state.get('refresh')])

  const unit = state.get('unit')

  const [val1, setVal1] = useState('')
  const [val2, setVal2] = useState('')
  const [val3, setVal3] = useState('')
  const [val4, setVal4] = useState('')
  const [val5, setVal5] = useState('')
  const [val6, setVal6] = useState('')
  const [val7, setVal7] = useState('')
  const [val8, setVal8] = useState('')
  const [error, setError] = useState('')

  const checkError = () => {
    setError("")
    if (isNaN(val1) || isNaN(val2) || isNaN(val3) || isNaN(val4) || isNaN(val5) || isNaN(val6) || isNaN(val7) || isNaN(val8)) {
      setError("Please enter numeric value(s).")
      return true
    } 
    // else if ((parseFloat(val1) + parseFloat(val2) + parseFloat(val3) + parseFloat(val4) + parseFloat(val5) + parseFloat(val6) + parseFloat(val7) + parseFloat(val8)) > 100) {
    //   setError("Total percentage could not be more than 100")
    //   return true
    // } 
    else if (val1 === '' ||
      val2 === '' ||
      val3 === '' ||
      val4 === '' ||
      val5 === '' ||
      val6 === '' ||
      val7 === '' ||
      val8 === '') {
      setError("Please enter value(s).")
      return true
    } 
    else if ((parseFloat(val1) === 0) &&
    (parseFloat(val2) === 0) &&
    (parseFloat(val3) === 0) &&
    (parseFloat(val4) === 0) &&
    (parseFloat(val5) === 0) &&
    (parseFloat(val6) === 0) &&
    (parseFloat(val7) === 0) &&
    (parseFloat(val8) === 0)) {
    setError("All values cannot be zero.")
    return true
  }
    return false
  }
  const handleButtonPress = () => {
    Keyboard.dismiss()
    let err = checkError()
    // alert(err)
    if (err === false) {
      // alert('in')
      addPigments([parseFloat(val1), parseFloat(val2), parseFloat(val3), parseFloat(val4), parseFloat(val5), parseFloat(val6), parseFloat(val7), parseFloat(val8)])
      navigation.navigate('Filler')
    }

  }
  return (
    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.goBack()}
        title="Add Pigments"
      />

      <View style={styles.container}>

        <View>
          <Text style={styles.titleText}>Enter the desired quantity of the following pigments</Text>
        </View>

        <KeyboardAvoidingView
          behavior="height" enabled
          style={{ flex: 1 }}
          keyboardVerticalOffset = {80}
        >

          <ScrollView
            showsVerticalScrollIndicator={false}
            marginVertical={5}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
          >
            <InputField title={"Red oxide"} value={val1} callBack={setVal1} unit={unit} />
            <InputField title={"Yellow oxide"} value={val2} callBack={setVal2} unit={unit} />
            <InputField title={"Black oxide"} value={val3} callBack={setVal3} unit={unit} />
            <InputField title={"Brown oxide"} value={val4} callBack={setVal4} unit={unit} />
            <InputField title={"Chromium green oxide"} value={val5} callBack={setVal5} unit={unit} />
            <InputField title={"Ultramarine blue oxide"} value={val6} callBack={setVal6} unit={unit} />
            <InputField title={"Ultramarine violet"} value={val7} callBack={setVal7} unit={unit} />
            <InputField title={"Gold mica"} value={val8} callBack={setVal8} unit={unit} />

          </ScrollView>
          {/* </KeyboardAvoidingView> */}
          {error
            ? <View style={{ justifyContent: 'center' }}><Text style={styles.error}>{error}</Text></View>
            : null
          }

          <View style={styles.button}>
            <Button buttonText="Next" btnWidth={280} callback={handleButtonPress} />
          </View>
          <View style={{ flex: 0.025 }} />

        </KeyboardAvoidingView>
      </View>

    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  titleText: {
    fontSize: 22,
    color: colors.purpleDark,
    textAlign: 'center'
  },
  error: {
    fontSize: 16,
    color: "red",
    textAlign: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 8,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
  },
});

const mapStateToProps = (state) => {
  return { state: state }
}

export default connect(mapStateToProps, actions)(PigmentScreen)
