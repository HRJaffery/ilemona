import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TextInput, Keyboard, ScrollView, Dimensions, Text } from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import Result from '../components/Result'
import { colors, pigmentNames, fillerNames } from '../configs'
import { saveFormula } from '../myService'
import { Dialog } from 'react-native-simple-dialogs';
import { Toast } from 'native-base';

const Results = ({ navigation, state, refreshApp }) => {

  const [showModal, setshowModal] = useState(false)
  const [name, setName] = useState('')
  const [data, setData] = useState([])

  const unit = state.get('unit')
  const pigments = state.get('pigments')
  const fillers = state.get('fillers')
  const finalPercentages = state.get('finalPercentages')

  useEffect(() => {
    let d = []
    let totalPigmentQty = 0;
    let totalFillerQty = 0;

    pigments.forEach((pigment, index) => {
      if(pigment !== 0){
        totalPigmentQty += pigment
      }
    });
    // console.log('totalPigmentQty',totalPigmentQty)
    pigments.forEach((pigment, index) => {
      let obj = {}
      obj['name'] = pigmentNames[index]
      if(pigment !== 0 ){
        // console.log('pigment',pigment)
        let value = (pigment/totalPigmentQty)*100
        // console.log('value',value)
        // console.log(finalPercentages[0])

        value = (value*finalPercentages[0])/100
        // console.log('Final',value)

        obj['value'] = value.toFixed(1)
      } else {
        obj['value'] = pigment
      }
      
      d.push(obj)
      
    });
    fillers.forEach((filler, index) => {
      if(filler !== 0){
        totalFillerQty += filler
      }
    });
    fillers.forEach((filler, index) => {
      let obj = {}
      obj['name'] = fillerNames[index]
      if(filler !== 0){
        let value = (filler/totalFillerQty)*100
      value = (value*finalPercentages[1])/100
      obj['value'] = value.toFixed(1)
      } else {
        obj['value'] = filler
      }
      d.push(obj)
  
    });
    setData(d)
  }, [state.get('pigments'),state.get('fillers'),state.get('finalPercentages')])

  const handleConfirmButtonPress = async () => {
    Keyboard.dismiss()
    setshowModal(false)
    let res = await saveFormula({ name: name, formula: data })
    await refreshApp(!(state.get('refresh')))
    showToast('Formula sucessfully saved.')
    navigation.navigate('AppHome')
  }

  const showToast = (message, duration = 3000) => {
    Toast.show({
      text: message,
      duration,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
      style: { alignSelf: 'center', backgroundColor: '#808080', paddingHorizontal: 10, marginBottom: 50, borderRadius: 25, width: 250 }
    });
  }
  const handleDiscardButtonPress = () => {
    refreshApp(!(state.get('refresh')))
    navigation.navigate('AppHome')
  }

  return (
    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.navigate('Final')}
        title="Results"
      />

      <View style={styles.container}>

        <Text style={styles.titleText}>Final Formula</Text>
        <ScrollView >
          {
            data.map(function (obj) {
              if (obj.value !== 0) {
                return <Result title={obj.name} value={obj.value} unit={unit} />
              }
            })
          }

        </ScrollView>

        <Dialog
          visible={showModal}
          title="Save Formula"
          onTouchOutside={() => setshowModal(false)}
          titleStyle={{ color: colors.purpleDark }}
          keyboardShouldPersistTaps={'handled'}
        >
          <View>
            <TextInput autoFocus={showModal} value={name} selectionColor={'black'} placeholder={'Enter Formula Name'} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize="none" autoCorrect={false} onChangeText={(name) => { setName(name) }} />

            <View style={styles.button}>

              <Button buttonText="Confirm" btnWidth={120} callback={handleConfirmButtonPress} />
              <View style={{ flex: 0.5 }}></View>

              <Button buttonText="Cancel" btnWidth={120} callback={() => setshowModal(false)} />
            
            </View>

          </View>
        </Dialog>

        <View style={styles.button} >
          <Button buttonText="Save" btnWidth={180} callback={() => setshowModal(true)} />

          <View style={{ flex: 0.5 }}></View>
          <Button buttonText="Discard" btnWidth={180} callback={handleDiscardButtonPress} />
        </View>

      </View>

    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  textInput: {
    marginBottom: 20,
    color: 'black',
    width: 250,
    borderWidth: 1,
    borderColor: 'white',
    borderBottomColor: colors.pinkLight,
    borderTopWidth: 0
  },
  titleText: {
    fontSize: 30,
    color: colors.purpleDark,
    textAlign: 'center',
    marginVertical: 10,
  },
  header: {
    flex: 1,
  },
  container: {
    flex: 9,
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return { state: state }
}
export default connect(mapStateToProps, actions)(Results)
