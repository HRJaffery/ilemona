import React from 'react';
import { View, StyleSheet, ScrollView, Dimensions } from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import Result from '../components/Result'

const ViewFormula = ({ navigation, state }) => {
  const id = navigation.getParam('id');

  const unit = state.get('unit')
  const savedFormulae = state.get('savedFormulae')
  const formula = savedFormulae[id].formula;

  return (
    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.navigate('MyFormulae')}
        title={savedFormulae[id].name}
      />

      <View style={styles.container}>

        <ScrollView >
          {
            formula.map(function (obj) {
              if (obj.value !== 0) {
                return <Result title={obj.name} value={obj.value} unit={unit} />
              }
            })
          }

        </ScrollView>

        <View style={styles.button} >
          <Button buttonText="Back" btnWidth={280} callback={() => navigation.navigate('MyFormulae')} />

        </View>

      </View>

    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  header: {
    flex: 1,
  },
  container: {
    flex: 9,
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10,
    marginTop: 10
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return { state: state }
}
export default connect(mapStateToProps, actions)(ViewFormula)
