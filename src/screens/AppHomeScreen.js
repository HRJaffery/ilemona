import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, Dimensions } from 'react-native'
import Button from '../components/Button'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { colors } from '../configs'
import LogoHeader from '../components/LogoHeader'
import { NavigationEvents } from 'react-navigation'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { TouchableOpacity } from 'react-native-gesture-handler'

const AppHomeScreen = ({ navigation, refreshApp, state }) => {

    return (
        <SafeAreaView style={styles.view}>
            <NavigationEvents            
                onWillFocus={() => {
                    refreshApp(!(state.get('refresh')))
                }}
            />
            <TouchableOpacity style={{margin: 5}} onPress={()=>navigation.openDrawer()}>
            <MaterialIcons name={'dehaze'} color={colors.purpleDark} size={40}/>
            </TouchableOpacity>
            <View style={styles.logo}>
                <LogoHeader />
            </View>

            <View style={styles.container}>
                <View style={styles.headingView}>

                    <Text style={styles.heading}>Select Product</Text>
                </View>
                <View style={styles.buttons}>
                    <Button buttonText={"Foundation\\Concealer"} btnWidth={280} callback={() => { navigation.navigate('Pigment') }} />
                    <Button buttonText={"Blush"} btnWidth={280} callback={() => { navigation.navigate('Pigment') }} />
                    <Button buttonText={"Eye Shadow"} btnWidth={280} callback={() => { navigation.navigate('Pigment') }} />

                </View>
                <View style={styles.button}>
                    <Button buttonText={"Your Formulae"} btnWidth={280} callback={() => { navigation.navigate('MyFormulae') }} />
                </View>
            </View>

            <View style={styles.headingView}>
            </View>
        </SafeAreaView>
    )
}
var { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: 'white'
    },
    heading: {
        fontSize: 40,
        color: colors.purpleLight,
        fontFamily: 'times'
    },
    logo: {
        flex: 5,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    header: {
        flex: 1
    },
    container: {
        flex: 9,
    },
    buttons: {
        flex: 1.5,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    button: {
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    headingView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

const mapStateToProps = (state) => {
    return { state: state }
  }

export default connect(mapStateToProps, actions)(AppHomeScreen)