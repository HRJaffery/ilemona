import React, { useEffect } from 'react'
import { View, StyleSheet, Image, AsyncStorage } from 'react-native'
import * as actions from '../actions'
import { connect } from 'react-redux'

const SplashScreen = ({ navigation, changeUnit }) => {

    const launch = async () => {

        return new Promise((resolve) =>

            setTimeout(async () => {
                let unit = await AsyncStorage.getItem("unit")
                let uid = await AsyncStorage.getItem('uid')
                if (unit) {
                    changeUnit(unit)
                }
                if(uid){
                    // console.log()
                    navigation.navigate('AppHome')    
                } else {
                    navigation.navigate('loginFlow')
                }
            }, 1500)
        );
    }

    useEffect(() => {
        launch()
    })

    return (

        <View style={styles.view}>
            <Image style={styles.image} source={require('../assets/imgpsh_fullsize_anim.jpg')} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    image: {
        width: '55%',
        height: '55%',
        aspectRatio: 1,
    }
})

export default connect(null, actions)(SplashScreen)