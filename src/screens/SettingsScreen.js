import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import Dropdown from '../components/Dropdown'

const Settings = ({ navigation }) => {

  const handleButtonPress = () => {
    navigation.navigate('AppHome')
  }

  const units = ['Kg', 'g', 'lb', 'oz']
  return (
    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.goBack()}
        title="Settings"
      />

      <View style={styles.container}>

        <View style={{ flex: 1, marginTop: 30 }}>
          <Dropdown options={units} />
        </View>

        <View style={styles.button}>
          <Button buttonText="Back" btnWidth={280} callback={handleButtonPress} />
        </View>

      </View>

    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  header: {
    flex: 1,
  },
  container: {
    flex: 9,
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10
  },
  button: {
    // marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default connect(null, actions)(Settings)
