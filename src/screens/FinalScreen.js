import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Keyboard, Dimensions, Text } from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import InputField from '../components/InputField'
import { colors } from '../configs'

const FinalScreen = ({ navigation, addFinalPercentages, state }) => {

  const [val1, setVal1] = useState('')
  const [val2, setVal2] = useState('')
  const [remaining, setRemaining] = useState(100)

  useEffect(()=>{
    setVal1(0)
    setVal2(0)
    setError('')
  },[state.get('refresh')])

  const [error, setError] = useState('')
  const checkError = (a,b) => {
    setError("")
    if (isNaN(a) || isNaN(b)) {
      setError("Please enter numeric value(s).")
      return true
    }else if ((parseFloat(a) + parseFloat(b)) > 100) {
      setError("Total percentage could not be more than 100")
      return true
    } else if ((parseFloat(a) + parseFloat(b)) < 100) {
      setError("Total percentage msut be equal to 100")
      return true
    }

    return false
  }
  const checkEmpty = () => {
    if (val1 === '' || val2 === '') {
      setError("Please enter value(s)")
      return true
    } 
  }
  const handleButtonPress = () => {
    checkEmpty()
    checkError(val1,val2)
    let err = checkError(val1,val2)
    if (err === false) {
      Keyboard.dismiss()
      addFinalPercentages([parseFloat(val1), parseFloat(val2)])
      navigation.navigate('Results')
    }
  }
  const changeVal1 = (val) => {
    setVal1(val)
    if (isNaN(val)) {
      setRemaining(100 - 0 - val2)
      return
    }
    if (isNaN(parseFloat(val))) {
      setRemaining(100 - 0 - val2)
      return
    }
    setRemaining(100 - parseFloat(val) - val2)
    checkError(val,val2)
  }
  const changeVal2 = (val) => {
    setVal2(val)
    if (isNaN(val)) {
      setRemaining(100 - 0 - val1)
      return
    }
    if (isNaN(parseFloat(val))) {
      setRemaining(100 - 0 - val1)
      return
    }
    setRemaining(100 - parseFloat(val) - val1)
    // console.log('aa',val)
    checkError(val1,val)
  }
  return (
    <View style={styles.view}>
      <View style={styles.header}>
        <Header
          leftIcon="arrow-back"
          leftCallback={() => navigation.navigate('Filler')}
          title=""
        />
      </View>
      <View style={styles.text}>
        <Text style={styles.titleText}>Compose Final Product</Text>

      </View>

      <View style={styles.container}>
        <InputField title={"Pigment"} value={val1} callBack={changeVal1} unit={"%"}  />
        <InputField title={"Filler/Base"} value={val2} callBack={changeVal2} unit={"%"} />



      </View>

      <View style={styles.textBottom}>
        <Text style={styles.bottomText}>(Total percentage must be equal to 100.)</Text>
      </View>
      <View style={styles.textBottom}>
        <Text style={styles.bottomText}>Remaining percentage: {remaining}%</Text>
      </View>
      <View style={styles.button}>
        <Button buttonText="Calculate" btnWidth={280} callback={handleButtonPress} />
      </View>
      <View style={styles.errorText}>
        {true
          ? <View style={{ justifyContent: 'center' }}><Text style={styles.error}>{error}</Text></View>
          : null
        }
      </View>

    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  titleText: {
    fontSize: 30,
    color: colors.purpleDark,
    textAlign: 'center'
  },
  bottomText: {
    color: colors.purpleDark,
    textAlign: 'center'
  },
  error: {
    fontSize: 16,
    color: "red",
    textAlign: 'center'
  },
  header: {
    flex: 1,
  },
  container: {
    flex: 2,
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10,
  },
  button: {
    flex: 1,
    alignItems: 'center',
  },
  textBottom: {
    flex: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  errorText: {
    flex: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'center',

  },
  text: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return { state: state }
}

export default connect(mapStateToProps, actions)(FinalScreen)
