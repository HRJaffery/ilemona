import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Text
} from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import FormulaCard from '../components/FormulaCard'
import { colors } from '../configs'
import { getSavedFormulae, deleteFormula } from '../myService'

const MyFormulae = ({ navigation, loadSavedFormulae, savedFormulae, refresh }) => {

  const [formulae, setFormulae] = useState({});
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(true);

  const deleteSelectedFormula = (id) => {
    deleteFormula(id)
    loadFormulae()
  }
  const viewFormula = (id) => {
    navigation.navigate('ViewFormula', { id })
  }
  const loadFormulae = async () => {
    if (savedFormulae) {
    }
    let res = null
    res = await getSavedFormulae()
    // console.log('RES',res)
    
    if (res === null) {
      setFormulae({})
      setMessage('There is no saved formula.')
    } else {
      setMessage('')
      loadSavedFormulae(res)
      setFormulae(res)
    }
    setLoading(false)
     

  }
  useEffect(() => {
    // console.log('RELOADING')
    loadFormulae()
  },[refresh])
  


  return (

    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.navigate('AppHome')}
        title="My Formulae"
      />

      <View style={styles.container}>

        {!loading
          ? 
          <>
            {message
              ? <View style={{ alignItems: 'center' }}>
              <Text style={{ color: colors.purpleDark }}>{message}</Text>
            </View>
              :
              <>
              <ScrollView >
              {
                Object.keys(formulae).map(function (key) {
                  return <FormulaCard title={formulae[key].name} id={key} viewCallback={viewFormula} deleteCallBack={deleteSelectedFormula} />
                })
              }

            </ScrollView>

            <View style={styles.button}>
              <Button buttonText="Back" btnWidth={280} callback={()=>navigation.navigate('AppHome')} />
            </View>
            </>
            }
          </>
          :
          <ActivityIndicator

            color={colors.purpleDark}
            size="large"
          />
        }
      </View>
    </View>

  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  container: {
    flex: 9,
    justifyContent: 'center',
    marginHorizontal: 15,
    marginBottom: 10,
    marginTop: 10,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return { savedFormulae: state.get('savedFormulae'), refresh: state.get('refresh') }
}
export default connect(mapStateToProps, actions)(MyFormulae)
