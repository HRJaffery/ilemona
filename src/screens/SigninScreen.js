import React, { useState } from 'react'
import { View, Text, StyleSheet, TextInput, SafeAreaView, Dimensions, TouchableOpacity } from 'react-native'
import Button from '../components/Button'
import LogoHeader from '../components/LogoHeader'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { colors } from '../configs'
import * as BaaS from '../myService'

const SigninScreen = ({ navigation }) => {

    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    const handleLoginPress = async () => {
        setLoading(true)
        setError(false)
        let status = await BaaS.signin(userName, password)
        if (status === true) {
            navigation.navigate('AppHome')
        } else {
            setLoading(false)
            setError(true)
        }
    }

    return (
        <SafeAreaView style={styles.view}>

            <View style={styles.logo}>
                <LogoHeader />
            </View>
            <View style={styles.container}>

                <View style={styles.top}>

                    <View style={styles.general}>
                        <TextInput value={userName} selectionColor={'black'} placeholder={'Email'} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize="none" autoCorrect={false} onChangeText={(userName) => { setUserName(userName) }} />
                        <TextInput value={password} secureTextEntry selectionColor={'black'} placeholder={'Password'} placeholderTextColor='#c7c7c7' style={styles.textInput} autoCapitalize="none" autoCorrect={false} onChangeText={(pass) => { setPassword(pass) }} />
                        {error
                            ? <Text style={styles.error}>Login Failed!. Please check your username and password and try again.</Text>
                            : null
                        }
                    </View>

                    <Button buttonText={"Sign In"} btnWidth={260} callback={() => handleLoginPress()} loading={loading} />

                </View>

                <View style={styles.bottom}>
                    <TouchableOpacity onPress={() => {
                        setUserName('')
                        setPassword('')
                        setError(false)
                        navigation.navigate('Signup')
                    }}>
                        <Text style={{ color: 'gray' }}>Dont have a account???{" "}
                            <Text style={styles.createAccountText}> Create new account</Text>
                        </Text>
                    </TouchableOpacity>

                </View>
            </View>
            <View style={styles.advertismentBottom}>

            </View>
        </SafeAreaView>
    )
}

var { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    view: {
        height: height,
        width: width,
        backgroundColor: 'white',
    },
    advertismentTop: {
        flex: 1,
        alignItems: 'center',
    },
    logo: {
        flex: 4.75,
    },
    container: {
        flex: 5,
    },
    top: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    general: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    error: {
        color: 'red',
        textAlign: 'center',
        paddingHorizontal: 35
    },
    space: {
        flex: 0
    },
    bottom: {
        flex: 0.5,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingHorizontal: 30,
    },
    createAccountText: {
        color: colors.pinkLight
    },
    textInput: {
        marginBottom: 20,
        color: 'black',
        width: 250,
        borderWidth: 1,
        borderColor: 'white',
        borderBottomColor: colors.pinkLight,
        borderTopWidth: 0
    },
    button: {
        flex: 1,
        marginVertical: 10,
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 16
    },
    txtBold: {
        fontWeight: 'bold'
    },
    advertismentBottom: {
        flex: 1.75,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default connect(null, actions)(SigninScreen)