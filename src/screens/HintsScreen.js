import React from 'react';
import {
  View,
  StyleSheet, 
  Dimensions,
  Text,
  ScrollView
} from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button'; 1
import { connect } from 'react-redux'
import * as actions from '../actions'
import { colors } from '../configs'

const HintsScreen = ({ navigation}) => {


  return (
    <View style={styles.view}>

      <Header
        leftIcon="arrow-back"
        leftCallback={() => navigation.navigate('AppHome')}
        title="Hints"
      />

      <View style={styles.container}>
    <ScrollView style={styles.innerContainer}
     showsVerticalScrollIndicator={false}
     >
        <View>
          <Text style={styles.titleText}>Some Helpful Hints</Text>
        </View>



          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>To create flesh tone: Red + Yellow + Blue OR Red + Yellow + Green.</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>To darken a tone, add brown or black.</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>To lighten a tone, add Titanium dioxide.</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>To improve coverage, add Titanium dioxide.</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>To create different shades of a particular tone, increase or decrease the ratio of filler/base.</Text>
          </View>

          <View>
          <Text style={styles.titleText}>Suggested Usage Rates</Text>
        </View>



          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Serecite = up to 100%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Rice powder = up to 100%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Silk powder = up to 100%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Talc = Controversial ingredient</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Corn starch = up to 100%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Zinc oxide powder = 5 - 25%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Titanium dioxide = 2 - 25%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Kaolin clay = up to 10%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <View>
            <Text style={styles.text}>Magnesium stearate = 10%</Text>
            <Text style={styles.text}>(less for ethnic skin)</Text>
            </View>
            
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Magnesium myristate = up to 20%</Text>
          </View>

          <View style={styles.textView}>
            <Text style={styles.textBullet}>{'\u2B24'} </Text>
            <Text style={styles.text}>Arrow root powder = up to 100%</Text>
          </View>

         
          </ScrollView>

        

        {/* </KeyboardAvoidingView> */}
      </View>
      <View style={styles.button}>
          <Button buttonText="Back" btnWidth={280} callback={()=>navigation.navigate('AppHome')} />
        </View>
        <View style={{ flex: 0.025 }} />
    </View>
  );
};
var { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: 'white',
    height: height,
    width: width,
  },
  titleText: {
    fontSize: 24,
    color: colors.purpleDark,
    textAlign: 'center',
    marginVertical:10
  },
  text: {
    flex: 1,
    fontSize: 18,
    color: colors.purpleDark,
    textAlign: 'justify',
    paddingHorizontal: 5,
    // borderColor: 'red',
    // borderWidth: 1
  },
  textBullet: {
    // flex: 1,
    fontSize: 18,
    color: colors.purpleDark,
    textAlign: 'justify',
    paddingHorizontal: 5,
    // borderColor: 'red',
    // borderWidth: 1
  },
  textView: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 10,
    // marginHorizontal: 10,
    justifyContent: 'flex-start',
    // borderColor: 'red',
    // borderWidth: 1
  },
  container: {
    flex: 1,
    marginHorizontal: 15,
    marginBottom: 10,
    // borderColor: 'green',
    // borderWidth: 1
  },
  innerContainer: {
    flex: 1,
    marginBottom: 10,
  
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
  },
});

const mapStateToProps = (state) => {
  return { state: state }
}

export default connect(mapStateToProps, actions)(HintsScreen)


