import {AsyncStorage} from 'react-native'
import firebase from '../../firebase'

export const signin = async(email, password) => {

  let status = false 
  await firebase.auth().signInWithEmailAndPassword(email, password)
  .then(async ()=>{
    await firebase.auth().currentUser.getIdToken(true).then( async (idToken)=>{
      status = true
      const uid = firebase.auth().currentUser.uid
      await AsyncStorage.setItem('uid',uid)
    })
    
  })
  .catch(()=>{
    status = false
  })
  return status
}

export const signOut = async(email, password) => {
  let status = false 
  await firebase.auth().signOut()
  .then(async ()=>{
    
    })
  .catch(()=>{
    status = false
  })
  return status
}

export const signup = async (email, password) => {
  let status = false 
  await firebase.auth().createUserWithEmailAndPassword(email,password)
  .then(async ()=>{
    await firebase.auth().currentUser.getIdToken(true).then( async function(idToken){
      status = true
      await AsyncStorage.setItem('token',idToken)
      const uid = firebase.auth().currentUser.uid
      await AsyncStorage.setItem('uid',uid)
     
    })
    
  })
  .catch(()=>{
    status = false
  })
  return status
}

export const saveFormula = async (formulae) => {
  // console.log(formulae)
  const uid = firebase.auth().currentUser.uid
  var newPostKey = firebase.database().ref().child(`/users/${uid}/formulae`).push().key;
  var updates = {};
  updates[`/users/${uid}/formulae` + newPostKey] = formulae;
  
  return firebase.database().ref().update(updates);
}

export const getSavedFormulae = async () => {
  const uid = await AsyncStorage.getItem('uid')
  let formulae = null
  if (uid !== null) 
  {
    await firebase.database().ref(`users/${uid}`).once('value', snapshot => {
      formulae = snapshot.val()
    })
  } 
  return formulae
}


export const deleteFormula = async (formulaID) => {
  const uid = firebase.auth().currentUser.uid
  firebase.database().ref(`/users/${uid}/${formulaID}`).
  remove().catch((err)=>alert(err)) 
}

export const addFillers = async (fillers) => {
  const uid = firebase.auth().currentUser.uid
  firebase.database().ref(`/fillers`).
  set(orders).catch((err)=>alert(err)) 
}
