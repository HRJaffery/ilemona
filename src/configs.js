// Android App id: ca-app-pub-7808265846149045~6513450922
export const addunitIDAndroidTop = "ca-app-pub-7808265846149045/6505712066"
export const addunitIDAndroidBottom = "ca-app-pub-7808265846149045/6745933954"

// iOS App id: ca-app-pub-7808265846149045~8797382225
export const addunitIDiOSTop = "ca-app-pub-7808265846149045/5224968721"
export const addunitIDiOSBottom = "ca-app-pub-7808265846149045/6314140373"

export const colors = {
    pinkDark: "#e69bc1",
    pinkLight: "#e8abd4",
    pinkLight2: "#f0cae4",
    purpleDark: "#bc54a0",
    purpleLight: "#d464a8",
    orange1: "#f47c7f",
    orange2: "#f56264",
    orange3: "#f24c54",
}

export const fillerNames = [
    "Sericite mica",
    "Arrowroot",
    "Silk",
    "Rice starch",
    "Kaolin clay",
    "Zinc oxide",
    "Titanium Dioxide",
    "Magnesium stearate",
    "Magnesium Myristate",
    "Corn starch",
]

export const pigmentNames = [
    "Red oxide",
    "Yellow oxide",
    "Black oxide",
    "Brown oxide",
    "Chromium green oxide",
    "Ultramarine blue oxide",
    "Ultramarine violet",
    "Gold mica",
 
]