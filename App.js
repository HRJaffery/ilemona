import React from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView, Dimensions } from 'react-native';
import StackNavigator from './src/StackNavigator'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import reducer from './src/reducer/reducer'
import {Root} from 'native-base'
import {colors} from './src/configs'

export default function App() {
  return ( 

    <Root>
      <Provider store={createStore(reducer)}>
        <StatusBar
          backgroundColor={colors.pinkLight}
          barStyle="light-content"
        />
        <SafeAreaView style={styles.safeview}>
          <StackNavigator/>
        </SafeAreaView>
      </Provider>
    </Root>
  );
}
var {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
  safeview: {
    flex: 1,
    backgroundColor: '#fff',
    height: height,
    width: width
  },
});
